# Automation Practice

### Assumptions
* Java 1.8 is installed and configured.
* maven is install and configured.
* Tests will be run locally on macOS Mojave (The latest chromedriver for mac is provided and can be found in src/test/resources/drivers ).

### Setup
* `cd` to you development directory.
* Clone project `git glone git@gitlab.com:djnwhiley/automation-practice.git` or `git clone https://gitlab.com/djnwhiley/automation-practice.git`.
* `cd automation-practice`.

### Run tests
* `mvn clean install`

### Configuration
* Tests will run against headless chrome, no browser will visibly load. `options.addArguments("headless");` can be commented out if you wish to see a browser load and interact with the web application.

